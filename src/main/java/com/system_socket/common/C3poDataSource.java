package com.system_socket.common;

import java.sql.Connection;
import java.sql.SQLException;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class C3poDataSource {
	 private static ComboPooledDataSource cpds = new ComboPooledDataSource();
	 
	    static {
	        try {
	        	
	            cpds.setDriverClass("com.mysql.jdbc.Driver");
	            cpds.setJdbcUrl(DbManager.url());
	            cpds.setUser(DbManager.dbUserName());
	            cpds.setPassword(new DESEncryption().decrypt_db(DbManager.password()) );
	            cpds.setMaxStatements(180);
	            cpds.setIdleConnectionTestPeriod(600);
	            cpds.setMaxIdleTime(1800);

	            cpds.setMinPoolSize(20);                                     
	            cpds.setAcquireIncrement(20);
	         

	          
	        } catch (Exception e) {
	            // handle the exception
	        }
	    }
	     
	    public static Connection getConnection() throws SQLException {
	        return cpds.getConnection();
	    }
	     
	    private C3poDataSource(){}
}
